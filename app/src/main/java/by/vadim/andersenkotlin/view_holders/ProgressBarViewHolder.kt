package by.vadim.andersenkotlin.view_holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import by.vadim.andersenkotlin.entity.ProgressBarEntity
import kotlinx.android.synthetic.main.progress_bar_item.view.*

class ProgressBarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(position: Int, bars: List<ProgressBarEntity>){
        itemView.apply {
            customProgressBar.progress = bars[position].progress
        }
    }
}