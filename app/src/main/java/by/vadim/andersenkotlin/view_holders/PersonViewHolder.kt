package by.vadim.andersenkotlin.view_holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import by.vadim.andersenkotlin.rows.IRow
import by.vadim.andersenkotlin.rows.PersonRow
import kotlinx.android.synthetic.main.about_person_list_item.view.*

class PersonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(position: Int, rows: List<IRow>) {
        val person: PersonRow = rows[position] as PersonRow
        itemView.apply {
            listFirstName.text = person.firstName
            listSecondName.text = person.secondName
        }
    }
}