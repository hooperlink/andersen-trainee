package by.vadim.andersenkotlin.view_holders

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import by.vadim.andersenkotlin.rows.CarRow
import by.vadim.andersenkotlin.rows.IRow
import kotlinx.android.synthetic.main.about_car_list_item.view.*

class CarViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(position: Int, rows: List<IRow>) {
        val car: CarRow = rows[position] as CarRow
        itemView.apply {
            listModel.text = car.model
            listHorsepower.text = car.horsePower.toString()
        }
    }
}