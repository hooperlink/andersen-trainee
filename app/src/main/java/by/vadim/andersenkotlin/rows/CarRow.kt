package by.vadim.andersenkotlin.rows

data class CarRow(val model: String, val horsePower: Int) : IRow