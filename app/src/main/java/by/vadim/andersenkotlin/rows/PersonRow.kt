package by.vadim.andersenkotlin.rows

data class PersonRow(val firstName: String, val secondName: String) : IRow