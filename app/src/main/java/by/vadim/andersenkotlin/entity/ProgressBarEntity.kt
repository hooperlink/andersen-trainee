package by.vadim.andersenkotlin.entity

data class ProgressBarEntity(var progress: Int = 0)