package by.vadim.andersenkotlin.activities

import android.os.Bundle
import by.vadim.andersenkotlin.R
import by.vadim.andersenkotlin.bundles.BundleGenerator.NAME
import kotlinx.android.synthetic.main.activity_show_name.*

class NameOutputActivity : MainActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intentWithName: String? by lazy {  intent.getStringExtra(NAME) }
        setContentView(R.layout.activity_show_name)
        showName.text = intentWithName
    }
 }