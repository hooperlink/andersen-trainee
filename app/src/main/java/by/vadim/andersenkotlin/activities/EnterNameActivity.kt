package by.vadim.andersenkotlin.activities

import android.os.Bundle
import android.widget.Toast
import by.vadim.andersenkotlin.R
import by.vadim.andersenkotlin.bundles.BundleGenerator
import kotlinx.android.synthetic.main.activity_enter_name.*

class EnterNameActivity : MainActivity() {

    companion object {
        const val ENTER_NAME = "enter name"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_name)
        sendName.setOnClickListener {
            if (inputName.text.isNullOrEmpty()) {
                Toast.makeText(applicationContext, ENTER_NAME, Toast.LENGTH_LONG).show()
            } else {
                val bundle = BundleGenerator.createBundleForNameOutPutActivity(inputName.text.toString())
                activityRouter.startNameOutputActivity(bundle)
            }
        }
    }
}