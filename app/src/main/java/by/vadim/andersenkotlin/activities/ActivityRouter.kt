package by.vadim.andersenkotlin.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import by.vadim.andersenkotlin.bundles.BundleGenerator


class ActivityRouter {

    private lateinit var context: Context;

    fun init(context: Context) {
       this.context = context
    }

    fun startNameOutputActivity(bundle: Bundle) {
        val nameIntent = Intent(context, NameOutputActivity::class.java)
        nameIntent.putExtra(BundleGenerator.NAME, bundle.getString(BundleGenerator.NAME))
        context.startActivity(nameIntent)
    }

    fun startEnterNameActivity() {
        val enterNameIntent = Intent(context, EnterNameActivity::class.java)
        context.startActivity(enterNameIntent)
    }

    fun startBottomNavigationBarActivity() {
        val navigationIntent = Intent(context, BottomNavigationBarActivity::class.java)
        context.startActivity(navigationIntent)
    }

}