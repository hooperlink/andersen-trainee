package by.vadim.andersenkotlin.activities

import android.os.Bundle
import android.os.PersistableBundle
import by.vadim.andersenkotlin.R
import kotlinx.android.synthetic.main.activity_bottom_navigation_bar.*

class BottomNavigationBarActivity : MainActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bottom_navigation_bar)
        if (savedInstanceState == null) {
            fragmentRouter.commitHomeFragment()
            bottomNavBar.selectedItemId = R.id.home
        }
        bottomNavBar.setOnNavigationItemSelectedListener {item ->
            when(item.itemId) {

                R.id.home -> {
                    fragmentRouter.commitHomeFragment()
               }

                R.id.about -> {
                    fragmentRouter.commitAboutFragment()
                }

                R.id.dashboard -> {
                    fragmentRouter.commitDashBoardFragment()
                }
            }
            true
        }
    }
}