package by.vadim.andersenkotlin.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import by.vadim.andersenkotlin.R
import kotlinx.android.synthetic.main.activity_main.*

open class MainActivity : AppCompatActivity() {

    lateinit var activityRouter: ActivityRouter
    lateinit var fragmentRouter: FragmentRouter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityRouter = ActivityRouter()
        activityRouter.init(this)
        fragmentRouter = FragmentRouter()
        fragmentRouter.init(this)
        setContentView(R.layout.activity_main)

        showEnterNameMenu.setOnClickListener {
            activityRouter.startEnterNameActivity()
        }

        navigationBar.setOnClickListener {
            activityRouter.startBottomNavigationBarActivity()
        }

    }
}