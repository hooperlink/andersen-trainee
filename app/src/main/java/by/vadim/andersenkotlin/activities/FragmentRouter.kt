package by.vadim.andersenkotlin.activities

import androidx.fragment.app.Fragment
import by.vadim.andersenkotlin.R
import by.vadim.andersenkotlin.fragments.AboutFragment
import by.vadim.andersenkotlin.fragments.DashboardFragment
import by.vadim.andersenkotlin.fragments.HomeFragment

class FragmentRouter {
    lateinit var mainActivity: MainActivity

    fun init(mainActivity: MainActivity) {
        this.mainActivity = mainActivity
    }

    fun commitHomeFragment() {
        commitFragmentToFragmentContainer(HomeFragment())
    }

    fun commitDashBoardFragment() {
        commitFragmentToFragmentContainer(DashboardFragment())
    }

    fun commitAboutFragment() {
        commitFragmentToFragmentContainer(AboutFragment())
    }

    private fun commitFragmentToFragmentContainer(fragment: Fragment) {
        mainActivity.supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, fragment).commit()
    }
}