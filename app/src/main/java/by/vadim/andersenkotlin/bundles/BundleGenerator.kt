package by.vadim.andersenkotlin.bundles

import android.os.Bundle


object BundleGenerator {

    const val NAME = "name"
    const val ROWS = "rows"

    fun createBundleForNameOutPutActivity(name: String) : Bundle {
        return Bundle().apply {
            putString(NAME, name)
        }
    }
}