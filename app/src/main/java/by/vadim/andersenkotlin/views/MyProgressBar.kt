package by.vadim.andersenkotlin.views

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import by.vadim.andersenkotlin.R

class MyProgressBar(context: Context, attributes: AttributeSet) : View(context, attributes) {
    private var progressPaint = Paint()
    private var fromZeroToFifteenProgressColor = 0
    private var fromSixteenToFiftyNinePercentOfProgressColor = 0
    private var fromSixtyToOneHundredPercentOfProgressColor = 0
    private var barHeight = 0F
    var progress = 0
    set(value) {
        field = value
        invalidate()
    }

    init {
        progressPaint.style = Paint.Style.FILL_AND_STROKE
        val typedArray : TypedArray = context.theme.obtainStyledAttributes(attributes, R.styleable.MyProgressBar, 0, 0)
        try{
            fromZeroToFifteenProgressColor = typedArray.getColor(R.styleable.MyProgressBar_fromZeroToFifteenProgressColor, Color.RED)
            fromSixteenToFiftyNinePercentOfProgressColor = typedArray.getColor(R.styleable.MyProgressBar_fromSixteenToFiftyNinePercentOfProgressColor, Color.YELLOW)
            fromSixtyToOneHundredPercentOfProgressColor = typedArray.getColor(R.styleable.MyProgressBar_fromSixtyToOneHundredPercentOfProgressColor, Color.GREEN)
            barHeight = typedArray.getDimension(R.styleable.MyProgressBar_barHeight, 10F)
        } finally {
            typedArray.recycle()
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val width = MeasureSpec.getSize(widthMeasureSpec)
        val heightSize = MeasureSpec.getSize(heightMeasureSpec)
        val height: Int
        height = when (MeasureSpec.getMode(heightMeasureSpec)) {
            MeasureSpec.EXACTLY -> heightSize
            MeasureSpec.AT_MOST -> 100.coerceAtMost(heightSize)
            else -> heightMeasureSpec
        }
        setMeasuredDimension(width, height)
    }

    override fun onDraw(canvas: Canvas?) {
        val halfHeight = height / 2
        val progressEndX: Int = (width * progress / 100F).toInt()
        progressPaint.strokeWidth = barHeight
        when(progress) {
            in 0..15 -> progressPaint.color = fromZeroToFifteenProgressColor
            in 16..59 -> progressPaint.color = fromSixteenToFiftyNinePercentOfProgressColor
            else -> progressPaint.color = fromSixtyToOneHundredPercentOfProgressColor
        }
        canvas?.drawLine(0F, halfHeight.toFloat(), progressEndX.toFloat(), halfHeight.toFloat(), progressPaint)
        progressPaint.color = Color.WHITE
        canvas?.drawLine(progressEndX.toFloat(),
            halfHeight.toFloat(), width.toFloat(), halfHeight.toFloat(), progressPaint)
    }
}