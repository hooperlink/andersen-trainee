package by.vadim.andersenkotlin.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import by.vadim.andersenkotlin.R
import by.vadim.andersenkotlin.entity.ProgressBarEntity
import by.vadim.andersenkotlin.view_holders.ProgressBarViewHolder

class ProgressBarAdapter (private val bars: List<ProgressBarEntity>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ProgressBarViewHolder(parent.inflate(R.layout.progress_bar_item))
    }

    override fun getItemCount(): Int = bars.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProgressBarViewHolder).bind(position, bars)
    }
}