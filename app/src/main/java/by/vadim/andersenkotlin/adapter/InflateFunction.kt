package by.vadim.andersenkotlin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

fun ViewGroup.inflate(layoutRes: Int, attachRoot : Boolean = false) : View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachRoot)
}