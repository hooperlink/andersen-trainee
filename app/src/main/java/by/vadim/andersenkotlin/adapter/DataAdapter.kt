package by.vadim.andersenkotlin.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import by.vadim.andersenkotlin.R
import by.vadim.andersenkotlin.rows.CarRow
import by.vadim.andersenkotlin.rows.IRow
import by.vadim.andersenkotlin.view_holders.CarViewHolder
import by.vadim.andersenkotlin.view_holders.PersonViewHolder
import java.lang.IllegalArgumentException

class DataAdapter (private val rows: List<IRow>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val CAR_TYPE = 0
        const val PERSON_TYPE = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : RecyclerView.ViewHolder {
        return when (viewType) {
            CAR_TYPE -> CarViewHolder(parent.inflate(R.layout.about_car_list_item))
            PERSON_TYPE -> PersonViewHolder(parent.inflate(R.layout.about_person_list_item))
            else -> throw IllegalArgumentException("No such type")
        }
    }

    override fun getItemCount(): Int = rows.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (rows[position] is CarRow) {
            (holder as CarViewHolder).bind(position, rows)
        } else {
            (holder as PersonViewHolder).bind(position, rows)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (rows[position] is CarRow) {
            CAR_TYPE
        } else {
            PERSON_TYPE
        }
    }
}