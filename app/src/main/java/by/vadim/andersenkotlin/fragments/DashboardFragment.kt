package by.vadim.andersenkotlin.fragments

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import by.vadim.andersenkotlin.R
import by.vadim.andersenkotlin.adapter.ProgressBarAdapter
import by.vadim.andersenkotlin.entity.ProgressBarEntity
import kotlinx.android.synthetic.main.dashboard_fragment.*

class DashboardFragment : Fragment() {
    companion object {
        val list = mutableListOf<ProgressBarEntity>()
    }
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.dashboard_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        progressBarRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = ProgressBarAdapter(list)
        }

        val handler = Handler(Looper.getMainLooper())
        addProgressBar.setOnClickListener {
            list.add(ProgressBarEntity())
            progressBarRecyclerView.adapter?.notifyItemInserted(list.size - 1)
            startProgressBar(list.size - 1, handler)
        }
    }

    private fun startProgressBar(position: Int, handler: Handler) {
        Thread(
            Runnable {
                while (list[position].progress < 100) {
                    handler.post {
                        list[position].progress++
                        progressBarRecyclerView.adapter?.notifyItemChanged(position)
                    }
                    Thread.sleep(100)
                }
            }
        ).start()
    }
}