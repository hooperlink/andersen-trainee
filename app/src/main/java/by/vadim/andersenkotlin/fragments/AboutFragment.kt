package by.vadim.andersenkotlin.fragments

import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import by.vadim.andersenkotlin.R
import by.vadim.andersenkotlin.adapter.DataAdapter
import by.vadim.andersenkotlin.rows.CarRow
import by.vadim.andersenkotlin.rows.IRow
import by.vadim.andersenkotlin.rows.PersonRow
import kotlinx.android.synthetic.main.about_fragment.*
import java.io.Serializable
import kotlin.collections.List as List

class AboutFragment : Fragment() {

    companion object {

        const val LIST = "list"

        private val rows = listOf(
            PersonRow("Vadzim", "Tamialoits"),
            CarRow("Fiat", 333),
            PersonRow("Masha", "Valenkova"),
            PersonRow("Pka", "Vafel"),
            CarRow("Ford Mustang", 600),
            CarRow("BMV", 450),
            CarRow("Mercedes", 222),
            PersonRow("Vasdim", "dasd"),
            PersonRow("Mark", "Olo"),
            CarRow("Ferrari", 530),
            CarRow("Moskvich", 2),
            CarRow("Bugatti", 320),
            PersonRow("Pet", "Parr"),
            PersonRow("Loew", "Male")
        )
    }

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.about_fragment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        aboutRecyclerView.apply {
            layoutManager = LinearLayoutManager(activity)
            adapter = if (savedInstanceState == null) {
                DataAdapter(rows)
            } else {
                val restoredRow = savedInstanceState.getSerializable(LIST) as List<IRow>
                DataAdapter(restoredRow)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putSerializable(LIST, rows as Serializable)
    }
}